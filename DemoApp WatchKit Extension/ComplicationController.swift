//
//  ComplicationController.swift
//  ComplicationsDemo WatchKit Extension
//
//  Created by Parrot on 2019-02-27.
//  Copyright © 2019 Parrot. All rights reserved.
//

import ClockKit
import WatchKit

class ComplicationController: NSObject, CLKComplicationDataSource {
    
    // MARK: - Timeline Configuration
    
    func getSupportedTimeTravelDirections(for complication: CLKComplication, withHandler handler: @escaping (CLKComplicationTimeTravelDirections) -> Void) {
        handler([.forward, .backward])
    }
    
    func getTimelineStartDate(for complication: CLKComplication, withHandler handler: @escaping (Date?) -> Void) {
        handler(nil)
    }
    
    func getTimelineEndDate(for complication: CLKComplication, withHandler handler: @escaping (Date?) -> Void) {
        
        
        //limit the timeline to be 24 hours from now
        var currentDate = Date()
        currentDate.addingTimeInterval(TimeInterval(24*60*60))
        
        handler(currentDate)
    }
    
    func getPrivacyBehavior(for complication: CLKComplication, withHandler handler: @escaping (CLKComplicationPrivacyBehavior) -> Void) {
        handler(.showOnLockScreen)
    }
    
    // MARK: - Timeline Population
    
    func getCurrentTimelineEntry(for complication: CLKComplication, withHandler handler: @escaping (CLKComplicationTimelineEntry?) -> Void) {
        // Call the handler with the current timeline entry
        
        if (complication.family == .modularLarge) {
            let myDelegate = WKExtension.shared().delegate as! ExtensionDelegate
            
            let template = CLKComplicationTemplateModularLargeStandardBody()
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "hh:mm"
            
            // update this to get data from app delegate
            let timeString = dateFormatter.string(from:myDelegate.pokemonList[0].start!)
            
            // update this to get data from app delegate
            // see lecture 1 from jawaad
            template.headerTextProvider = CLKSimpleTextProvider(text:myDelegate.pokemonList[0].name!)
            template.body1TextProvider = CLKSimpleTextProvider(text:"NONSENSE 1")
            template.body2TextProvider = CLKSimpleTextProvider(text:"NONSENSE 2")
            
            
            let timelineEntry = CLKComplicationTimelineEntry(date:myDelegate.pokemonList[0].start!, complicationTemplate:template)

            
            handler(timelineEntry)
        }
        else {
            handler(nil)
        }
    }
    
    
    // conveninece funciton to get all entries to include in the timeline
    func getList() -> [CLKComplicationTimelineEntry] {
        
        // create an array to represent all future entries you want to show in the timeline
        
        let myDelegate = WKExtension.shared().delegate as! ExtensionDelegate
        var timelineEntryArray = [CLKComplicationTimelineEntry]()
        var nextDate = Date(timeIntervalSinceNow:60*60)
        for i in 1...(myDelegate.pokemonList.count-1) {
            // loop through the reminaining items in the array
            let template = CLKComplicationTemplateModularLargeStandardBody()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "hh:mm"
            
            
            print("Index \(i), Current pokemon: \(myDelegate.pokemonList[i].name)")
            let timeString = dateFormatter.string(from:myDelegate.pokemonList[i].start!)
            
            template.headerTextProvider = CLKSimpleTextProvider(text:myDelegate.pokemonList[i].name!)
            template.body1TextProvider = CLKSimpleTextProvider(text:"PPPPP 1")
            template.body2TextProvider = CLKSimpleTextProvider(text:timeString)
            
            let timelineEntry = CLKComplicationTimelineEntry(date:myDelegate.pokemonList[i].start!, complicationTemplate:template)
            
            timelineEntryArray.append(timelineEntry)
            nextDate = nextDate.addingTimeInterval(60*60)
            
        }
        return timelineEntryArray
    }
    
    
    
    func getTimelineEntries(for complication: CLKComplication, before date: Date, limit: Int, withHandler handler: @escaping ([CLKComplicationTimelineEntry]?) -> Void) {
        // Call the handler with the timeline entries prior to the given date
        
        
        
        // code to create the list of entries BEFORE the current time
        if (complication.family == .modularLarge) {
            var timelineEntries = [CLKComplicationTimelineEntry]()
            timelineEntries = self.getList()
            handler(timelineEntries)
        } else {
            handler(nil)
        }
        
        
    }
    
    func getTimelineEntries(for complication: CLKComplication, after date: Date, limit: Int, withHandler handler: @escaping ([CLKComplicationTimelineEntry]?) -> Void) {
        // Call the handler with the timeline entries after to the given date
     
        
        // list of entires to show events AFTER The current time
        // COPY AND PASTE of previous function ! no thinking!
        if (complication.family == .modularLarge) {
            var timelineEntries = [CLKComplicationTimelineEntry]()
            timelineEntries = self.getList()
            handler(timelineEntries)
        } else {
            handler(nil)
        }
        
        
        
        
    }
    
    // MARK: - Placeholder Templates
    
//    func getLocalizableSampleTemplate(for complication: CLKComplication, withHandler handler: @escaping (CLKComplicationTemplate?) -> Void) {
//        // This method will be called once per supported complication, and the results will be cached
//        handler(nil)
//    }
    func getPlaceholderTemplate(for complication: CLKComplication, withHandler handler: @escaping (CLKComplicationTemplate?) -> Void) {
        let template = CLKComplicationTemplateModularLargeStandardBody()
        template.headerTextProvider = CLKSimpleTextProvider(text:"Header TEST Message")
        
        template.body1TextProvider = CLKSimpleTextProvider(text:"Sub message 1")
        template.body2TextProvider = CLKSimpleTextProvider(text:"Sub message 2")
        handler(template)
    }
    
    
}
