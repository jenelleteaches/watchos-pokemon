//
//  ExtensionDelegate.swift
//  DemoApp WatchKit Extension
//
//  Created by Parrot on 2019-02-26.
//  Copyright © 2019 Parrot. All rights reserved.
//

import WatchKit

class ExtensionDelegate: NSObject, WKExtensionDelegate {

    // putting stuff here makes it accesible to toher parts of the code
    var pokemonList:[PokemonObject] = []
    
    func applicationDidFinishLaunching() {
//        // Perform any final initialization of your application.
//
        // Create pokemon and add them to the array
        let p1 = PokemonObject()
        var startTime = p1.end?.addingTimeInterval(TimeInterval(30))
        var endTime = startTime?.addingTimeInterval(TimeInterval(30))

        p1.printDates()
        let p2 = PokemonObject(name: "Snorlax", image: "002-fatpanda", start:startTime!, end: endTime!)

        p2.printDates()

        startTime = p2.end?.addingTimeInterval(TimeInterval(30))
        endTime = startTime?.addingTimeInterval(TimeInterval(30))

        let p3 = PokemonObject(name:"Bulbasaur", image:"003-bulba",start:startTime!, end: endTime!)
        p3.printDates()
        
        pokemonList.append(p1)
        pokemonList.append(p2)
        pokemonList.append(p3)

        for p in pokemonList {
            print(p)
        }




    }

    func applicationDidBecomeActive() {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillResignActive() {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, etc.
    }

    func handle(_ backgroundTasks: Set<WKRefreshBackgroundTask>) {
        // Sent when the system needs to launch the application in the background to process tasks. Tasks arrive in a set, so loop through and process each one.
        for task in backgroundTasks {
            // Use a switch statement to check the task type
            switch task {
            case let backgroundTask as WKApplicationRefreshBackgroundTask:
                // Be sure to complete the background task once you’re done.
                backgroundTask.setTaskCompletedWithSnapshot(false)
            case let snapshotTask as WKSnapshotRefreshBackgroundTask:
                // Snapshot tasks have a unique completion call, make sure to set your expiration date
                snapshotTask.setTaskCompleted(restoredDefaultState: true, estimatedSnapshotExpiration: Date.distantFuture, userInfo: nil)
            case let connectivityTask as WKWatchConnectivityRefreshBackgroundTask:
                // Be sure to complete the connectivity task once you’re done.
                connectivityTask.setTaskCompletedWithSnapshot(false)
            case let urlSessionTask as WKURLSessionRefreshBackgroundTask:
                // Be sure to complete the URL session task once you’re done.
                urlSessionTask.setTaskCompletedWithSnapshot(false)
            default:
                // make sure to complete unhandled task types
                task.setTaskCompletedWithSnapshot(false)
            }
        }
    }

}
