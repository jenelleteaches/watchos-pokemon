//
//  PokemonObject.swift
//  DemoApp WatchKit Extension
//
//  Created by Parrot on 2019-02-27.
//  Copyright © 2019 Parrot. All rights reserved.
//

import WatchKit

class PokemonObject: NSObject {
    
    var name:String?
    var image:String?
    var start:Date?
    var end:Date?
//    func initWithData(name:String?, image:String?) {
//        self.name = name
//        self.image = image
//    }
    
    convenience override init() {
        // default
        var startTime = Date()
        // add 2 minutes to startTime
        var endTime = startTime.addingTimeInterval(TimeInterval(2.0 * 60.0))
        self.init(name: "Pikachu", image:"001-pikachu", start:startTime, end:endTime)
    }
    
    init(name: String, image:String, start:Date, end:Date) {
        self.name = name
        self.image = image
        self.start = start
        self.end = end
    }
    
    func printDates() {
        print("\(self.name)")
        print("Start: \(self.start)")
        print("End: \(self.end)")
        print("------")
    }
    
}
