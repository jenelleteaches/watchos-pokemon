//
//  PokemonRowController.swift
//  DemoApp
//
//  Created by Parrot on 2019-02-27.
//  Copyright © 2019 Parrot. All rights reserved.
//

import WatchKit

class PokemonRowController: NSObject {
    
    @IBOutlet var rowImage: WKInterfaceImage!
    @IBOutlet var rowNameLabel: WKInterfaceLabel!
}
