//
//  TableExampleInterfaceController.swift
//  DemoApp WatchKit Extension
//
//  Created by Parrot on 2019-02-27.
//  Copyright © 2019 Parrot. All rights reserved.
//

import WatchKit
import Foundation


class TableExampleInterfaceController: WKInterfaceController {
    
    
    // MARK: Outlets
    @IBOutlet var pokemonTable: WKInterfaceTable!
    
    // Create a static data source
    let stringData = ["Warm-up", "Cardio", "Weightlifting", "Core", "Bike", "Cooldown"]
    
    let imageData = ["001-pikachu", "002-fatpanda", "003-bulba", "004-bullb", "005-jiggly", "006-eevee"]

    // get the pokemon list from the delegte
    
    
    func createPokemon() {
        
    }
    
    func loadTableData() {
        print("trying to load data")
        let myDelegate = WKExtension.shared().delegate as! ExtensionDelegate
    
        pokemonTable.setNumberOfRows(myDelegate.pokemonList.count,withRowType: "PokemonRowController")
        
        print("myDelegate count: \(myDelegate.pokemonList.count)")
        
        
        for (index, pokemon) in myDelegate.pokemonList.enumerated() {
            print("Index: \(index), Pokemon: \(pokemon.name)")
            let row = pokemonTable.rowController(at: index)
                as! PokemonRowController
            row.rowNameLabel.setText(pokemon.name)
            row.rowImage.setImage(UIImage(named: pokemon.image!))
        }
    }
    
    override init() {
        super.init()
        
        // create the pokemon objects
        // this is dynamically building the data source
        self.createPokemon()
        
        // load them into the table
        self.loadTableData()
    }
    
    // MARK: Default functions
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
